from pricing.conf import Config
import requests



class ProductManager(object):

    @classmethod
    def get_params(cls, id_produto, model_name):
        url = Config.get("ProductManager", "URI")
        url = url + "/pricing/" + id_produto + "/" + model_name + "/params"
        req = requests.get(url)
        if req.status_code != 200:
            raise Exception("Problema com o Product Manager...")

        req_json = req.json()
        return req_json

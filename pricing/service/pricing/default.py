import re
import numpy as np
from pricing.service.scoring.default import DefaultScoring
from pricing.service.pricing.base import BasePricing
from pricing.repo.clients import ClientRepo
from pricing.utils import remover_outlier
from scipy import optimize
from werkzeug import exceptions


class DefaultPricing(BasePricing):
    def __init__(self, params):
        self.params = params
        self.params_ret = self.get_params_pattern('retencao')
        self.params_rating = self.get_params_rating()
        self.params_tx = self.get_params_pattern('tx_')
        self.params_prazo = self.get_params_pattern('prazoMin')
        self.meses_validos = None

    def validar_dados(self, dados):
        ds = DefaultScoring.validar_dados(dados)

        if 'custo_trava' not in dados:
            raise exceptions.BadRequest("Missing 'custo_trava' field")

        if 'volume_desejado' not in dados:
            raise exceptions.BadRequest("Missing 'volume_desejado' field")

    def get_params_pattern(self, pattern):
        dfinal = {}
        for chave in self.params:
            if re.search(pattern, chave):
                dfinal[chave] = self.params[chave]
        return dfinal

    # monta dicionario de listas a partir do dicionario raw
    def get_params_rating(self, pattern='rating'):
        print(self.params)
        dfinal = {}
        for chave in self.params:
            if chave[0:6] != pattern:
                continue
            cat = chave.split("_")[0][-1:]
            if dfinal.get(cat, None) == None:
                if re.search("upper", chave):
                    dfinal[cat] = [self.params[chave], 0]
                else:
                    dfinal[cat] = [0, self.params[chave]]
            else:
                arr = np.array(dfinal[cat])
                pos = np.where(arr == 0)[0][0]
                dfinal[cat][pos] = self.params[chave]

        return dfinal


    #Mapeia os scores em ratings discretos. Determinar se vai ser assim ou continuo(Necessaio interpolacao do percentual de ret)
    def map_score(self, score):
        ret = ''
        for chave in self.params_rating:
            if self.params_rating[chave][1] == 0:
                scoreMin = chave

            if (score > self.params_rating[chave][1]) and (score <= self.params_rating[chave][0]):
                ret = chave

        if len(ret) == 0:
            ret = scoreMin
        return ret

    def scoring(self, infos):
        id_produto = infos["id_produto"]
        params_score = ClientRepo.get_model_params(id_produto, "DefaultScoring")
        params_score["base"] = self.params["base"]
        sd = DefaultScoring(params_score)
        score = sd.calcular(infos)
        score = score["score"]
        self.meses_validos = sd.meses_validos

        if score is None:
            return None

        rating = self.map_score(score)
        return rating

    def percentualRet(self,rating):
        ret = self.params_ret.get("retencao{}".format(rating))
        return ret

    def compute_custo_trava(self, fat_medio, custo_trava, tx_retencao, vol_operacao, tx_captalys_desejada, prazo_estimado):
        def _ctir(n_per, tx_retencao, fat_medio, custo_trava):
            fluxo = n_per * [(tx_retencao * fat_medio) - (fat_medio * custo_trava)]
            fluxo = [-vol_operacao] + fluxo
            tir = np.irr(np.array(fluxo))
            return tir

        # converter taxa para semanal
        ndias = 52 / 12
        tx_retencao = (1 + tx_retencao) ** (1 / ndias) - 1
        tx_captalys_desejada = (1 + tx_captalys_desejada) ** (1 / ndias) - 1
        custo_trava = (1 + custo_trava) ** (1 / ndias) - 1

        n_per = int(np.round(prazo_estimado * ndias))
        diff_old = 10

        while True:
            tir = _ctir(n_per, tx_retencao, fat_medio, custo_trava)
            tir_mes = ((tir + 1) ** ndias - 1)
            diff = abs(tx_captalys_desejada - tir)

            if diff > diff_old:
                tir_cedente = _ctir(n_per, tx_retencao, fat_medio, 0)
                tir_cedente = ((tir_cedente + 1) ** ndias - 1)
                return tir_mes, tir_cedente, np.round(n_per / ndias)

            if diff <= 1e-5:
                break
            else:
                diff_old = diff
                n_per += 1

        tir_cedente = _ctir(n_per, tx_retencao, fat_medio, 0)
        tir_cedente = ((tir_cedente + 1) ** ndias - 1)
        return tir_mes, tir_cedente, np.round(n_per / ndias)

    def calcular_propostas_parciais(self, fat_medio, vol_desejado, tx_captalys_desejada, tx_retencao_rating, rating):
        prazo_min = self.params['prazo_minimo']
        prazo_rating = self.params.get("prazo{}".format(rating))
        custo_processamento = self.params["custo_processamento"]

        _vol_desejado = vol_desejado * (1 + custo_processamento)

        prazo_int = (prazo_rating + prazo_min) / 2
        propostas = []

        for prazo in [prazo_min, prazo_int, prazo_rating]:
            _pmt = -np.pmt(nper=prazo, rate=tx_captalys_desejada, pv=_vol_desejado)
            _tx_retencao = _pmt / fat_medio
            _devido = _pmt * prazo

            if _tx_retencao <= tx_retencao_rating:
                propostas.append({"devido": _devido, "pmt": _pmt, "tx_retencao": _tx_retencao,
                                  "vol_desejado": _vol_desejado,
                                  "prazo_parcial": prazo,
                                  "tx_captalys_desejada": tx_captalys_desejada})

        return propostas

    def calcular_propostas_finais(self, propostas_parciais, custo_trava, fat_medio):
        lista_propostas = []
        for prop in propostas_parciais:
            novas_taxas = self.compute_custo_trava(fat_medio, custo_trava, prop["tx_retencao"], prop["vol_desejado"],
                                                   prop["tx_captalys_desejada"], prop["prazo_parcial"])
            prazo_novo = novas_taxas[2]
            devido_novo = prop["pmt"] * prazo_novo

            prop["devido_novo"] = devido_novo
            prop["tx_cedente"] = novas_taxas[1]
            prop["tx_captalys"] = novas_taxas[0]
            prop["custo_trava"] = custo_trava
            prop["prazo_novo"] = prazo_novo
            lista_propostas.append(prop)

        return lista_propostas

    def calcular_fluxo_min(self, prazo_estimado, rating, fat_medio):
        denum = self.params_prazo.get("prazoMin{}".format(rating)) + prazo_estimado
        num = prazo_estimado
        perc_min = num / denum
        return perc_min * fat_medio

    def calcular(self, data, periodicidade="mensal"):
        volume_desejado = data["volume_desejado"]
        dados = data["dados"][0:self.meses_validos]
        rating = self.scoring(data)
        custo_trava = data["custo_trava"]
        custo_processamento = self.params["custo_processamento"]
        limite_valor_proposta = self.params["limite_valor_proposta"]

        if rating is None:
            return {"rating": None,
                    "taxa_captalys": None,
                    "taxa_cedente": None,
                    "tx_retencao": None,
                    "prazo_novo": None,
                    "prazo": None}

        dados_novos = remover_outlier(dados, np.mean(dados), np.std(dados))
        fat_medio = np.mean(dados_novos)
        prazo_rating = self.params.get("prazo{}".format(rating))
        tx_retencao = self.percentualRet(rating)

        tx_captalys_desejada = self.params_tx.get("tx_captalys{}".format(rating))
        max_fat = -np.pv(tx_captalys_desejada, prazo_rating, (fat_medio * tx_retencao), 0) * (1 - custo_processamento)

        propostas_parciais = self.calcular_propostas_parciais(fat_medio, volume_desejado, tx_captalys_desejada, tx_retencao, rating)
        propostas_finais = self.calcular_propostas_finais(propostas_parciais, custo_trava, fat_medio)
        fluxos_min = [self.calcular_fluxo_min(el["prazo_novo"], rating, fat_medio) for el in propostas_finais]

        for i, el in enumerate(propostas_finais):
            el["fluxo_min"] = round(fluxos_min[i] / 100) * 100
            el["valor_max"] = min(np.floor(max_fat / 5000) * 5000, limite_valor_proposta)
            el["custo_processamento"] = volume_desejado * self.params["custo_processamento"]
            el["valor_credito"] = volume_desejado + el["custo_processamento"]
            el["tx_retencao"] = round(el["tx_retencao"], 4)
            el["devido_novo"] = round(el["devido_novo"] / 100) * 100
            el["custo_fixo"] = el["devido_novo"] - el["valor_credito"]
            el["tx_retencao_rating"] = tx_retencao
            el["fat_medio"] = fat_medio
            el["fat_max"] = int(np.max(dados))
            el["fat_min"] = int(np.min(dados))
            el["custo_total"] = el["custo_processamento"] + el["custo_fixo"]

        propostas_dummy = 3 - len(propostas_finais)
        for _ in range(propostas_dummy):
            propostas_finais.append({"tx_retencao_rating": tx_retencao, "tx_retencao": 1})

        return propostas_finais

from pricing.service.pricing.default import DefaultPricing
import numpy as np


class MoipPricing(DefaultPricing):

    def compute_custo_trava(self, fat_medio, custo_trava, tx_retencao, vol_operacao, tx_captalys_desejada, prazo_estimado):
        def _ctir(n_per, tx_retencao, vol_operacao, custo_trava):
            tir = np.rate(n_per, (fat_medio * tx_retencao * (1 - custo_trava)), -vol_operacao, 0)
            return tir

        n_per = prazo_estimado
        diff_old = 10

        while True:
            tir = _ctir(n_per, tx_retencao, vol_operacao, custo_trava)
            diff = abs(tx_captalys_desejada - tir)

            if diff > diff_old:
                tir_cedente = _ctir(n_per, tx_retencao, vol_operacao, 0)
                return tir, tir_cedente, n_per

            if diff <= 1e-5:
                break
            else:
                diff_old = diff
                n_per += 0.001

        tir_cedente = _ctir(n_per, tx_retencao, vol_operacao, 0)
        return tir, tir_cedente, n_per

 # coding = utf-8

import abc

class BaseScoring:

    @abc.abstractmethod
    def calcular(self, data):
        pass

    @abc.abstractproperty
    def model_name(self):
        pass

    @abc.abstractmethod
    def validar_dados(self, data):
        pass

from pricing.service.scoring.base import BaseScoring
from conector.mysql import CaptalysCon
from pricing.utils import remover_outlier, get_params_rating, find_value_params
import pandas as pd
import numpy as np
from werkzeug import exceptions


class DefaultScoring(BaseScoring):
    """
    Enviar dados ordenados e devidamente resampleados mensalmente.
    """
    def __init__(self, params):
        self.params = params
        self.params_pos = get_params_rating(params, "variacaoPositiva")
        self.params_neg = get_params_rating(params, "variacaoNegativa")
        self.params_fluxo_max = get_params_rating(params, "fluxoMax")
        self.base = params["base"]

    def verificaTipoProposta(self, dados):
        if (np.any(dados[0:6] == 0)) or (len(dados) < 6):
            self.meses_validos = None
            return {"meses": None}
        elif np.any(dados[6:] == 0):
            self.meses_validos = 6
            return {"meses": 6}
        elif (len(dados) <= 12) and (len(dados) >= 6):
            self.meses_validos = 6
            return {"meses": 6}
        else:
            self.meses_validos = 12
            return {"meses": 12}

    def variacao_faturamento(self, dados, qtd_meses):
        desvio = np.std(dados[0:qtd_meses])
        media = np.mean(dados[0:qtd_meses])
        num = remover_outlier(dados[0:int(qtd_meses/2)], media, desvio)
        denom = remover_outlier(dados[int(qtd_meses/2):qtd_meses], media, desvio)
        var = np.mean(num) / np.mean(denom) - 1
        return var

    def calcular_setor_localizacao(self, cnae):
        cc = CaptalysCon(self.base)
        mysql_con = cc.connect()
        query = "select * from {}.tb_Setor where cnae = '{}'".format(self.base,cnae)
        res = mysql_con.execute(query).fetchone()
        if res != None:
            res = float(res[2])
        mysql_con.close()
        return res

    def calcular_localizacao(self,cnpj):
        cc = CaptalysCon(self.base)
        mysql_con = cc.connect()
        query = """ select cid.score
                from {0}.tb_Empresas as emp
                inner join {0}.tb_Endereco as ende
                on ende.idEndereco = emp.idEndereco
                inner join {0}.tb_CidadeScore as cid
                on cid.cidade = ende.cidade
                where emp.cnpj = '{1}' """.format(self.base,cnpj)


        res = mysql_con.execute(query).fetchone()
        if res == None:
            score = res[0]
        else:
            score = 0.75
        mysql_con.close()
        return score


    def get_nota_coef_variacao(self,fluxo_fat,variacao_fat, params_pos, params_neg):
        if fluxo_fat >= 0:
            chave = find_value_params(abs(variacao_fat), params_pos)
        elif fluxo_fat < 0:
            chave = find_value_params(abs(variacao_fat), params_neg)
        else:
            raise Exception("Valor calculado inadequado")
        nota = int(chave) / 100
        return nota


    def get_nota_variacao_fat(self,variacao, params_fluxo_max):
        chave = find_value_params(variacao, params_fluxo_max)
        nt = int(chave)/100
        return nt


    def calcular(self, data):
        dados = data["dados"]
        cnae = data["cnae"]
        dados = np.array(dados)
        cnpj = data["cnpj"]
        tipoProposta = self.verificaTipoProposta(dados)

        if tipoProposta["meses"] is None:
            return {"score": None}

        qtd_meses = tipoProposta["meses"]
        variacao_fat = self.variacao_faturamento(dados, qtd_meses)
        score_setor = self.calcular_setor_localizacao(cnae)
        score_localizacao = self.calcular_localizacao(cnpj)

        if score_setor is None:
            return {"score": None}

        media = np.mean(dados[0:qtd_meses])
        desvio = np.std(dados[0:qtd_meses])
        dados_novos = remover_outlier(dados, media, desvio)
        variacao = np.std(dados_novos) / np.mean(dados_novos)

        # obter notas
        nt_coef_variacao = self.get_nota_coef_variacao(variacao_fat,variacao, self.params_pos, self.params_neg)
        nt_variacao_fat = self.get_nota_variacao_fat(variacao_fat,self.params_fluxo_max)

        # score
        score = self.params['peso-coef-variacao'] * nt_coef_variacao + self.params['peso-coef-delta-faturamento'] * nt_variacao_fat
        score = score + self.params['peso-coef-setor-localizacao'] * ((score_setor + score_localizacao)*0.5)
        return {"score": score}

    @classmethod
    def validar_dados(cls, data):
        if data is None:
            raise exceptions.BadRequest("Missing data")

        if 'cnae' not in data:
            raise exceptions.BadRequest("Missing 'cnae' field")

        if 'dados' not in data:
            raise exceptions.BadRequest("Missing 'dados' field")

        if type(data['dados']) is not list:
            raise exceptions.UnprocessableEntity("Field 'dados' should be an array")


if __name__ == '__main__':
    params = {"variacaoPositiva_100_upper": 1,
      "variacaoPositiva_100_lower": 0.9,
      "variacaoPositiva_75_upper": 0.9,
      "variacaoPositiva_75_lower": 0.8,
      "variacaoPositiva_50_upper": 0.8,
      "variacaoPositiva_50_lower": 0.7,
      "variacaoPositiva_0_lower": 0,
      "variacaoPositiva_0_upper": 0.7,
      "variacaoNegativa_100_upper": 1,
      "variacaoNegativa_100_lower": 0.9,
      "variacaoNegativa_75_upper": 0.9,
      "variacaoNegativa_75_lower": 0.8,
      "variacaoNegativa_50_upper": 0.8,
      "variacaoNegativa_50_lower": 0.7,
      "variacaoNegativa_0_lower": 0,
      "variacaoNegativa_0_upper": 0.7,
          "tx_captalys_A": 0.04,
          "tx_captalys_B": 0.05,
          "tx_captalys_C": 0.06,
          "tx_captalys_D": 0.07,
          "tx_captalys_E": 0.08,
          "ratingA_upper": 1,
          "ratingA_lower": 0.9,
          "ratingB_upper": 0.9,
          "ratingB_lower": 0.8,
          "ratingC_upper": 0.8,
          "ratingC_lower": 0.7,
          "ratingD_upper": 0.7,
          "ratingD_lower": 0.6,
          "ratingE_upper": 0.6,
          "ratingE_lower": 0.0,
          "fluxoMax_100_upper": 99999,
          "fluxoMax_100_lower": -0.15,
          "fluxoMax_75_upper": -0.15,
          "fluxoMax_75_lower": -0.25,
          "fluxoMax_50_upper": -0.25,
          "fluxoMax_50_lower": -0.4,
          "fluxoMax_25_upper": -0.4,
          "fluxoMax_25_lower": -0.6,
          "fluxoMax_0_upper":  -0.6,
          "fluxoMax_0_lower": 0,
         "peso-coef-variacao":0.2,
          "peso-coef-setor-localizacao":0.3,
          "peso-coef-delta-faturamento":0.5}

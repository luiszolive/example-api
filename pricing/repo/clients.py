# coding = utf-8

import requests

from pricing.conf import Config
from pricing.utils.errors import handle_request_error

class ClientRepo():

    base_path = Config.get("ProductManager", "URI")

    @classmethod
    def get_models_by_client(cls, client_id):
        uri = "{}/{}/pricing".format(cls.base_path, client_id)

        resp = requests.get(uri)
        if resp.status_code != 200:
            handle_request_error(resp.status_code, resp.json(), "client")

        return resp.json()

    @classmethod
    def get_model_params(cls, product_id, model_name):
        uri = "{}/{}/pricing/{}/params".format(cls.base_path, product_id, model_name)

        resp = requests.get(uri)
        if resp.status_code != 200:
            handle_request_error(resp.status_code, resp.json(), "model params")

        return resp.json()

# coding = utf-8

from werkzeug import exceptions

def handle_request_error(status=500, body={}, entity=""):
    if status == 404:
        raise exceptions.NotFound(body)
    else:
        raise exceptions.InternalServerError("Internal Server Error", body)

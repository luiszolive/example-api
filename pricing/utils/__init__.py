import re
import numpy as np

def remover_outlier(dados, media, desvio):
    res1 = [x for x in dados if (x >= (media - 1* desvio)) and (x <= (media + 1 * desvio))]
    return res1

# monta dicionario de listas a partir do dicionario raw
def get_params_rating(params, pattern='rating'):
    dfinal = {}
    for chave in params:
        if chave[0:len(pattern)] != pattern:
            continue
        cat = chave.split("_")[1]
        if dfinal.get(cat, None) == None:
            if re.search("upper", chave):
                dfinal[cat] = [params[chave], "str"]
            else:
                dfinal[cat] = ["str", params[chave]]
        else:
            arr = np.array(dfinal[cat])
            pos = np.where(arr == "str")[0][0]
            dfinal[cat][pos] = params[chave]
    return dfinal


def find_value_params(value, params):
	ret = ''
	for chave in params:
		if params[chave][1] == 0:
			scoreMin = chave
		if (value > params[chave][1]) and (value <= params[chave][0]):
			ret = chave

	if len(ret) == 0:
		ret = scoreMin
	return ret

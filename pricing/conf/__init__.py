import configparser as _configparser
from os import getenv as _getenv
from os import path as _path


config_dir = _path.dirname(__file__) # Absolute path to the directory this file is in
env = _getenv('ENV', 'development')

Config = _configparser.ConfigParser()
Config.read(_path.join(config_dir, env, 'conf.ini'))

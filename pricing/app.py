# coding = utf-8

from flask import Flask, Blueprint

from pricing.conf import Config
from pricing.api import api
from pricing.api.scoring import ns as model_namespace

app = Flask(__name__)
blueprint = Blueprint('api', __name__)

api.init_app(blueprint)
api.add_namespace(model_namespace, '/models')
app.register_blueprint(blueprint)


if __name__ == "__main__":
    host = Config.get('Flask', 'host')
    port = Config.get('Flask', 'port')
    debug = Config.get('Flask', 'debug')
    app.run(host, int(port), debug)

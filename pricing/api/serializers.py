# coding = utf-8

from flask_restplus import fields, reqparse

auth_parser = reqparse.RequestParser()
auth_parser.add_argument('X-Consumer-Custom-Id', required=True, location='headers')
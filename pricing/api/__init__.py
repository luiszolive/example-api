# coding = utf-8

import logging
import os

from flask_restplus import Api as _Api
from flask_restplus import reqparse as _reqparse
from werkzeug.exceptions import HTTPException

log = logging.getLogger(__name__)

v = os.popen('git log | head -n 1')
commit = v.read().replace("commit ", "")[:7]

api = _Api(version='0.1#{}'.format(commit), title='API Pricing Captalys', description='API centralizada de precificação')

@api.errorhandler
def default_error_handler(e):
    if isinstance(e, HTTPException):
        print(e.get_response())
        response = {'message': e.description}
        status_code = e.code
    else:
        response = {'message': 'Unhandled Exception'}
        status_code = 500

    log.exception(e)
    return response, status_code

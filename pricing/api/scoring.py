# coding = utf-8

import logging

from flask import Blueprint, jsonify, request
from werkzeug import exceptions
from flask_restplus import Resource

import pricing.service as service
from pricing.repo.clients import ClientRepo
from pricing.api.serializers import auth_parser
from pricing.api import api


# score_handlers = Blueprint("score_handlers", __name__)
log = logging.getLogger(__name__)

ns = api.namespace('models', description='Operações relacionadas à scoring')


m = api.model('body', {})
@ns.route('/<string:model_name>')
class Model_caller(Resource):

    @ns.expect(auth_parser, m)
    @ns.response(code=400, description='Bad Request')
    @ns.response(code=404, description='Model not found')
    def post(self, model_name):
        # pegar produto do header
        id_produto = auth_parser.parse_args()['X-Consumer-Custom-Id']

        # find the model
        checkModel = hasattr(service, model_name)

        if not checkModel:
            raise exceptions.NotFound("There are not models with the name {}".format(model_name))

        cls = getattr(service, model_name)
        data = request.json

        if data is None:
            data = {}

        # get params
        params = ClientRepo.get_model_params(id_produto, model_name)
        params["base"] = id_produto

        # instanciando
        inst = cls(params)

        # executando
        data["id_produto"] = id_produto
        inst.validar_dados(data)
        ret = inst.calcular(data)

        return ret

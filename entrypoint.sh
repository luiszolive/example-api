#!/bin/bash

cd /app/pricing/pricing

gunicorn app:app --bind 0.0.0.0:5001 -w 4 --reload

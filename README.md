# API Pricing Captalys


O repositório contém modelos de Scoragem e Precificação desenvolvidos pelo time
de Data Science e Analytics da Captalys. Abaixo segue o manual de como utilizar
os modelos desenvolvidos.



# Lista de modelos disponiveis.

    1. Model name: DefaultScoring
       Body request:
       ```json
       {
           "dados": array([float]),
           "cnae": string
       }
       ```

    2. Model name: DefaultPricing
       Body request:
       ```json
       {
           "dados": array([float]),
           "cnae": string,
           "volume_desejado": float,
           "custo_trava": float
       }
       ```
    
    
# Step 1 - Cadastro do Produto.
...configurar usando o product manager... #TODO



No fim da etapa de cadastro do produto você receberá um PRODUCTID que deverá ser
utilizado durante as requisições na API.


# Step 2 - Cadastro dos Parâmetros.



# Step 3 - Exemplo de uso.

### Pricing

``` html
POST /models/DefaultPricing
header -- X-Consumer-Custom-Id: PRODUCTID
body
```
# api-example

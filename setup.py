from setuptools import setup

# install requirements of the project
with open("requirements.txt") as req:
    install_requires = req.read()

setup(name='pricing',
      version="0.0.2",
      description="Pricing api",
      url="https://gitlab.com/captalysTech/api-pricing",
      author="Phillip Beck",
      author_email="phillip.beck@captalys.com.br",
      license="BSD",
      keywords="captalys technology",
      packages=["pricing"],
      zip_safe=False,
      install_requires=install_requires
      ),

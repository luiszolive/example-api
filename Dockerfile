FROM 569805827224.dkr.ecr.sa-east-1.amazonaws.com/calpine:python
LABEL mantainer="luis.oliveira@captalys.com.br"
LABEL fileversion=v0.1

WORKDIR /app
WORKDIR pricing

COPY . /app/pricing

RUN mkdir ~/.ssh && mv conector-db-key ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa && \
    chmod 600 ~/.ssh/id_rsa && touch ~/.ssh/known_hosts && \ 
    ssh-keyscan gitlab.com >> ~/.ssh/known_hosts && \
    git clone ssh://git@gitlab.com/captalysTech/conector-db.git && \ 
    cd conector-db && python3 setup.py develop && \
    pwd && ls -la && mv /app/pricing/conector-db/conector/conf/production/config.ini.example /app/pricing/conector-db/conector/conf/production/config.ini && \
    mv /app/pricing/conector-db/conector/conf/development/config.ini.example /app/pricing/conector-db/conector/conf/development/config.ini && python3 setup.py develop && \
    cd .. && \
    git clone ssh://git@gitlab.com/infraCaptalys/weddell.git && \ 
    cd weddell && ls -la && python3 setup.py develop && \
    cd ..  

RUN python setup.py develop

ENTRYPOINT ["/bin/sh","/app/pricing/entrypoint.sh"]
